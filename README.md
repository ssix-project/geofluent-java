# GeoFluent Java Client

Unofficial Java Client to access the [GeoFluent API](http://www.lionbridge.com/geofluent/). 

It has been developed by [Redlink GmbH](http://redlink.co/) in the context of the [SSIX Project](https://ssix-project.eu/);
further details at [this blog post](https://ssix-project.eu/opensourcing-java-and-python-clients-geofluent-api/).

## API

Upon usage of the [GeoFluent API](http://www.lionbridge.com/geofluent/) you **need to register** an account there.

You can find further technical documentation at:

* [API v3 Swagger Documentation](https://api.geofluent.com/Translation/swagger/docs/v3)
* [API v3 Swagger UI](https://api.geofluent.com/Translation/swagger/ui/index)

The current status of implementation of the [GeoFluent API](http://www.lionbridge.com/geofluent/)
is the following:

| Feature  	        |Status   	|
|---	            |---	    |
| Detect  	        | **implemented** |
| HealthCheck  	    | not implemented |
| Interpret  	    | not implemented |
| Languages         | **implemented** |
| Profile           | not implemented |
| TermsOfService    | not implemented |
| Transcribe        | **implemented** |

## Installation

The library is [available from Maven Central](http://search.maven.org/#search%7Cga%7C1%7Cg%3A%22io.redlink.ssix.geofluent%22),
so you just need to declare the dependency at your `pom.xml` file:

    <dependency>
        <groupId>io.redlink.ssix.geofluent</groupId>
        <artifactId>geofluent-java</artifactId>
        <version>0.2.0</version>
    </dependency>
    
## Testing

You can run the integration tests using your credentials using the following command:

    mvn integration-test -Dgeofluent.key=*** -Dgeofluent.secret=***
	
## Usage

    final GeoFluentClient geoFluentClient =  new GeoFluentClient(key, secret);
	geoFluentClient.init();
	
	// retrieve available languages
	final Map<String, String> languages = geoFluentClient.languages();
	System.out.println("Supported languages: " + languages.entrySet().stream()
                                                          .map(entry -> entry.getKey() + "->"+ entry.getValue())
                                                          .collect(joining(", ")));
    
	// translate from German to English
	final String translation = geoFluentClient.translate("Ich werde es testen", "de-de", "en-xn");
	System.out.println("Translation: " + translation);
															

## License

This tool is available under [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0).

## Disclaimer

Notice that this implementation is **not an official client supported by the API provider**, so it is available without 
any warranty nor liability. Please report to the issue trackers any issue you may find to improve upcoming releases.

## Acknowledgements

This work is in part funded by the [SSIX](http://ssix-project.eu/) [Horizon 2020](https://ec.europa.eu/programmes/horizon2020/) project
(grant agreement No 645425).

[![H2020](https://bytebucket.org/ssix-project/brexit-gold-standard/raw/master/assets/eu.png)](https://ec.europa.eu/programmes/horizon2020/)
