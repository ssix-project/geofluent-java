/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.redlink.ssix.geofluent.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import java.util.Date;

/**
 * Basic JWT internal utilities
 *
 * @author Sergio Fernández
 */
public class JWTUtils {

    public static final int DEFAULT_EXPIRATION_TIME = 8 * 60 * 60;
    public static final SignatureAlgorithm DEFAULT_SIGNATURE_ALGORITHM = SignatureAlgorithm.HS256;

    /**
     * Encode JWT token with default expiration time and algorithm
     *
     * @param key api key
     * @param secret api secret
     * @return token
     */
    public static String encodeToken(String key, String secret) {
        return encodeToken(key, secret, DEFAULT_SIGNATURE_ALGORITHM);
    }

    /**
     * Encode JWT token with default algorithm
     *
     * @param key api key
     * @param secret api secret
     * @param iat issued time
     * @param exp expiration time
     * @return token
     */
    public static String encodeToken(String key, String secret, Date iat, Date exp) {
        return encodeToken(key, secret, iat, exp, SignatureAlgorithm.HS256);
    }

    /**
     * Encode JWT token with default expiration time
     *
     * @param key api key
     * @param secret api secret
     * @param algorithm signing algorithm
     * @return token
     */
    public static String encodeToken(String key, String secret, SignatureAlgorithm algorithm) {
        long nowMillis = System.currentTimeMillis();
        final Date iat = new Date(nowMillis);
        final Date exp = new Date(nowMillis + DEFAULT_EXPIRATION_TIME);
        return encodeToken(key, secret, iat, exp, algorithm);
    }

    /**
     * Encode JWT token
     *
     * @param key api key
     * @param secret api secret
     * @param iat issued time
     * @param exp expiration time
     * @param algorithm signing algorithm
     * @return token
     */
    public static String encodeToken(String key, String secret, Date iat, Date exp, SignatureAlgorithm algorithm) {
        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(secret);
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, algorithm.getJcaName());

        JwtBuilder builder = Jwts.builder()
                .setIssuedAt(iat)
                .setExpiration(exp)
                .setSubject(key)
                .signWith(algorithm, signingKey);

        return builder.compact();
    }

    /**
     * Decode JWT token
     *
     * @param token jwt token
     * @param secret api secret
     * @return api key
     */
    public static String decodeToken(String token, String secret) {
        final Claims claims = Jwts.parser()
                .setSigningKey(DatatypeConverter.parseBase64Binary(secret))
                .parseClaimsJws(token).getBody();
        return claims.getSubject();
    }

}
