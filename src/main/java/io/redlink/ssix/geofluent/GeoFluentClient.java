/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.redlink.ssix.geofluent;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.redlink.ssix.geofluent.utils.JWTUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

/**
 * GeoFluent Java Client
 *
 * @author Sergio Fernández
 */
public class GeoFluentClient implements Serializable {

    private static final Logger log = LoggerFactory.getLogger(GeoFluentClient.class);

    public static final String BASE = "https://api.geofluent.com/Translation";
    public static final int VERSION = 3;
    public static final int DEFAULT_EXPIRATION = 8 * 60;
    private String key;

    private String secret;
    private String token;
    private Date exp;
    private Map<String, String> headers;
    private int expirationTimeframe;

    private transient CloseableHttpClient httpClient;
    private ObjectMapper mapper;

    /**
     * Default construction, key and secret must be set later on
     *
     */
    public GeoFluentClient() {
        this(null, null);
    }

    /**
     * Default construction with custom expiration time, key and secret must be set later on
     *
     * @param expiration expiration time
     */
    public GeoFluentClient(int expiration) {
        this(null, null, expiration);
    }

    /**
     * Construct a GeoFluent client
     *
     * @param key api key
     * @param secret api secret
     */
    public GeoFluentClient(String key, String secret) {
        this(key, secret, DEFAULT_EXPIRATION);
    }

    /**
     * Construct a GeoFluent client
     *
     * @param key api key
     * @param secret api secret
     * @param expiration token expiration time
     */
    public GeoFluentClient(String key, String secret, int expiration) {
        super();
        this.expirationTimeframe = expiration;
        this.key = key;
        this.secret = secret;
        this.mapper = new ObjectMapper();
    }

    /**
     * Initializes the GeoFluent client, generating a valid JWT token within the current time period
     *
     */
    public void init() {
        if (StringUtils.isBlank(key) || StringUtils.isBlank(secret)) {
            throw new IllegalStateException("GeoFluentClient has not been properly initialize, key or secret are missing");
        }

        final Date iat = new Date();
        final Date exp = DateUtils.addMinutes(iat, expirationTimeframe);
        this.token = JWTUtils.encodeToken(key, secret, iat, exp);
        this.exp = exp;
        this.headers = new HashMap<String, String>() {{
            put("User-Agent", "GeoFluent Java Client");
            put("Content-Type", "application/json");
            put("Accept", "application/json");
            put("Authorization", String.format("Bearer %s", token));
        }};
        this.mapper = new ObjectMapper();
        this.httpClient = HttpClients.createDefault();
    }

    /**
     * Destroys the client
     *
     */
    public void destroy() {
        this.token = null;
        this.exp = null;
        this.headers = null;
        this.httpClient = null;
    }

    /**
     * Retrieve supported pair of languages
     *
     * @return pair of languages supported
     * @throws IOException api request error
     * @throws URISyntaxException api url encoding issues
     */
    public Map<String, String> languages() throws IOException, URISyntaxException {
        healthCheck(); //TODO replace by interceptor or so
        return get("Languages", Collections.emptyMap(), response -> {
            if (response.getStatusLine().getStatusCode() == 200) {
                final Map<String, String> languages = new HashMap<>();
                final Map<String, List<Map<String, Map<String, String>>>> results = mapper.readValue(response.getEntity().getContent(),
                        new TypeReference<Map<String, List<Map<String, Map<String, String>>>>>() {});
                for (Map<String, Map<String, String>> result : results.get("result")) {
                    languages.put(result.get("source").get("code"), result.get("target").get("code"));
                }
                log.debug("Languages: {}", languages);
                return languages;
            } else {
                log.error("Request failed with {}: {}", response.getStatusLine().getStatusCode(), response.getStatusLine().getReasonPhrase());
                return Collections.emptyMap(); //FIXME
            }
        });
    }

    /**
     * Translate content from the source language to the target (if supported)
     *
     * @param content content to translate
     * @param source content's language
     * @param target target language for the translation
     * @return translation
     * @throws IOException api request error
     * @throws URISyntaxException api url encoding issues
     */
    public String translate(String content, String source, String target) throws IOException, URISyntaxException {
        healthCheck(); //TODO replace by interceptor or so
        final Map<String,String> params = new HashMap<String, String>() {{
            put("text", content);
            put("from", source);
            put("to", target);
        }};
        return get("Translate", params, response -> {
            if (response.getStatusLine().getStatusCode() == 200) {
                final Map<String, List<Map<String, String>>> results = mapper.readValue(response.getEntity().getContent(),
                        new TypeReference<Map<String, List<Map<String, String>>>>() {});
                return results.get("result").get(0).get("text");
            } else {
                log.error("Request failed with {}: {}", response.getStatusLine().getStatusCode(), response.getStatusLine().getReasonPhrase());
                return ""; //FIXME
            }
        });
    }

    public String detectLanguage(String content) throws IOException, URISyntaxException {
        healthCheck(); //TODO replace by interceptor or so
        final Map<String,String> params = new HashMap<String, String>() {{
            put("text", content);
        }};
        return get("Detect", params, response -> {
            if (response.getStatusLine().getStatusCode() == 200) {
                final Map<String, List<String>> results = mapper.readValue(response.getEntity().getContent(),
                        new TypeReference<Map<String, List<String>>>() {});
                if (results.containsKey("result") && results.get("result").size() > 0) {
                    final String language = results.get("result").get(0);
                    //normalization to the country code
                    if (language.contains("-")) {
                        return language.split("-")[0];
                    } else {
                        return language;
                    }
                } else {
                    log.warn("Illegal API interaction: request returned {} without the expected result in the response",
                            response.getStatusLine().getStatusCode());
                    return null;
                }
            } else {
                log.error("Request failed with {}: {}", response.getStatusLine().getStatusCode(), response.getStatusLine().getReasonPhrase());
                return ""; //FIXME
            }
        });
    }

    /**
     * Runs an internal health check, if necessary, for the validity of the current JWT token
     *
     */
    private void healthCheck() {
        if (exp == null || new Date(System.currentTimeMillis()).after(exp)) {
            log.debug("Performing health check on client token...");
            init();
        } else if (mapper == null) {
            log.warn("ObjectMapper was null!!!");
            mapper = new ObjectMapper();
        }

    }

    /**
     * Generic HTTP GET request
     *
     * @param endpoint url
     * @param params query parameters
     * @param responseHandler handler to process the response
     * @param <T> templated return type
     * @return processed response
     * @throws URISyntaxException
     * @throws IOException
     */
    private <T> T get(String endpoint, Map<String,String> params, ResponseHandler<T> responseHandler) throws URISyntaxException, IOException {
        final String resource = buildResource(endpoint);
        final URIBuilder builder = new URIBuilder(resource);
        for (Map.Entry<String, String> entry: params.entrySet()) {
            builder.setParameter(entry.getKey(), entry.getValue());
        }
        final URI uri = builder.build();
        final HttpGet get = new HttpGet(uri);
        for (Map.Entry<String, String> header: headers.entrySet()) {
            get.addHeader(header.getKey(), header.getValue());
        }
        if (httpClient == null) {
            log.warn("PIPELINE-252: httpClient is null!!!"); //FIXME
            httpClient = HttpClients.createDefault();
        }
        log.debug("Requesting '{}'...", uri);
        return httpClient.execute(get, responseHandler);
    }

    /**
     * Build resource's url
     *
     * @param endpoint geofluent endpoint
     * @return url
     */
    private String buildResource(String endpoint) {
        return String.format("%s/v%d/%s", BASE, VERSION, endpoint);
    }

    /**
     * Get api key
     *
     * @return api key
     */
    public String getKey() {
        return key;
    }

    /**
     * Set api key
     *
     * @param key api key
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * Get api secret
     *
     * @return api secret
     */
    public String getSecret() {
        return secret;
    }

    /**
     * Set api secret
     *
     * @param secret api secret
     */
    public void setSecret(String secret) {
        this.secret = secret;
    }

    /**
     * Set custom Jackson's ObjectMapper instance
     *
     * @param mapper object mapper
     */
    public void setMapper(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    /**
     * Get expiration time
     *
     * @return expiration time
     */
    public int getExpirationTimeframe() {
        return expirationTimeframe;
    }

    /**
     * Set expiration time
     *
     * @param expirationTimeframe expiration time in seconds
     */
    public void setExpirationTimeframe(int expirationTimeframe) {
        this.expirationTimeframe = expirationTimeframe;
    }

    /**
     * Get expiration datetime
     *
     * @return date of current expiration
     */
    public Date getExp() {
        return exp;
    }

}
