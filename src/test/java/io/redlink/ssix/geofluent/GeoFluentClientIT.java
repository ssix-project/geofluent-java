/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.redlink.ssix.geofluent;

import org.junit.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static org.hamcrest.text.IsEqualIgnoringCase.equalToIgnoringCase;
import static org.junit.Assert.assertThat;

/**
 * GeoFluentClient integration tests
 *
 * @author Sergio Fernández
 */
public class GeoFluentClientIT {

    private static GeoFluentClient client;
    private static String key;
    private static String secret;

    @BeforeClass
    public static void beforeClass() {
        key = System.getProperty("geofluent.key");
        secret = System.getProperty("geofluent.secret");
        Assume.assumeNotNull(key);
        Assume.assumeNotNull(secret);
        client = new GeoFluentClient(key, secret);
    }

    @Before
    public void setup() {
        client.init();
    }

    @After
    public void teardown() {
        client.destroy();
    }

    @Test(expected = IllegalStateException.class)
    public void testNoInitException() throws IOException, URISyntaxException {
        client = new GeoFluentClient();
        Assert.assertFalse(client.languages().isEmpty());
    }

    @Test
    public void testNoInit() throws IOException, URISyntaxException {
        client = new GeoFluentClient();
        client.setKey(key);
        client.setSecret(secret);
        Assert.assertFalse(client.languages().isEmpty());
    }

    @Test
    public void testLanguages() throws IOException, URISyntaxException {
        final Map<String, String> languages = client.languages();
        Assert.assertNotNull(languages);
        Assert.assertFalse(languages.isEmpty());
        Assert.assertTrue(languages.size() >= 5);
    }

    @Test
    public void testEmptyTranslate() throws IOException, URISyntaxException {
        Assert.assertNotNull(client.translate(null, null, null));
    }

    @Test
    public void testTranslates() throws IOException, URISyntaxException {
        final String EN = "en-xn";
        final String DE = "de-de";

        final Map<String, String> translations = new HashMap<String, String>() {{
            put("this is a test", "das ist ein Test");
            put("but good", "aber gut");
            put("I will test it", "Ich werde es testen");
        }};

        for (Map.Entry<String, String> entry: translations.entrySet()) {
            assertThat(client.translate(entry.getKey(), EN, DE), equalToIgnoringCase(entry.getValue()));
        }

        final Map<String, String> invertedTranslations = translations.entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getValue, c -> c.getKey()));
        for (Map.Entry<String, String> entry: invertedTranslations.entrySet()) {
            assertThat(client.translate(entry.getKey(), DE, EN), equalToIgnoringCase(entry.getValue()));
        }
    }

    @Test
    public void testHealthCheck() throws IOException, URISyntaxException, InterruptedException {
        final int timeframe = 1;
        client.setExpirationTimeframe(timeframe);
        client.init();

        Assert.assertFalse(client.languages().isEmpty());
        Thread.sleep(((timeframe*60)+1)*1000);
        Assert.assertFalse(client.languages().isEmpty());
    }

    @Test
    public void testDetectLanguages() throws IOException, URISyntaxException {
        final Map<String, String> sample = new HashMap<String, String>() {{
            put("this is a test", "en");
            put("das ist ein Test", "de");
            put("but good", "en");
            put("aber gut", "de");
            put("I will test it", "en");
            put("Ich werde es testen", "de");
            put("esto es una prueba", "es");
        }};

        for (Map.Entry<String, String> entry: sample.entrySet()) {
            assertThat(client.detectLanguage(entry.getKey()), equalToIgnoringCase(entry.getValue()));
        }
    }

}
