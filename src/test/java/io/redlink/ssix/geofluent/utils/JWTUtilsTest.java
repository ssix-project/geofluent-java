/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.redlink.ssix.geofluent.utils;

import org.junit.Assert;
import org.junit.Test;

/**
 * Basic JWT tests
 *
 * @author Sergio Fernández
 */
public class JWTUtilsTest {

    public static final String KEY = "57ab6824-f579-2235-98d4-34fdc579a567";
    public static final String SECRET = "Ooyohsh3loaz4ahva9ii6shoh2aef3eeLei5eivaiph2";

    @Test
    public void testEncodeToken() {
        final String token = JWTUtils.encodeToken(KEY, SECRET);
        Assert.assertNotNull(token);
    }

    @Test
    public void testEncodeDecodeToken() {
        final String token = JWTUtils.encodeToken(KEY, SECRET);
        Assert.assertNotNull(token);

        final String key = JWTUtils.decodeToken(token, SECRET);
        Assert.assertNotNull(key);
        Assert.assertEquals(KEY, key);
    }

}
